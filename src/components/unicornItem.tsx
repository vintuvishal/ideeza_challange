import React from "react";
import { Box, Typography, IconButton, Modal, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/DeleteOutline";
import EditIcon from "@material-ui/icons/EditOutlined";
import { useDispatch } from "react-redux";
import { deleteUnicorn } from "../redux/actions/data";
import { UnicornPopUp } from "./unicornPopUP";

const useStyles = makeStyles(({ palette }) => ({
  modalbox: {
    outline: "none",
  },
  image: {
    borderRadius: "50%",
    height: "30px",
    width: "30px",
  },
  fieldBox: {
    width: "170px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  root: {
    backgroundColor: palette.primary.light,
    borderBottom: `1px solid #D6D8E7`,
    color: palette.secondary.dark,
    fontWeight: 600,
  },
}));

export const UnicornItem = (props: { unicorn: any, index: number }) => {
  const { unicorn } = props;
  const classes = useStyles();

  const dispatch = useDispatch();
  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleDelete = () => {
    dispatch(deleteUnicorn(unicorn._id));
  };

  return (
    <>
      <Grid
        container
        className={classes.root}
        direction="row"
        alignItems="center"
        justify="center"
      >
        <Box className={classes.fieldBox}>
          <Typography variant="h6">{unicorn.name}</Typography>
        </Box>
        <Box className={classes.fieldBox}>
          <Typography variant="h6">{unicorn.age}</Typography>
        </Box>
        <Box className={classes.fieldBox}>
          <Typography variant="h6">{unicorn.colour}</Typography>
        </Box>

        <Box
          display="flex"
          className={classes.fieldBox}
          flexDirection="row"
          px={1}
        >
          <Typography>
            <IconButton aria-label="edit" onClick={handleOpen}>
              <EditIcon color="primary" />
            </IconButton>
          </Typography>
          <Typography>
            <IconButton aria-label="delete" onClick={handleDelete}>
              <DeleteIcon color="error" />
            </IconButton>
          </Typography>
        </Box>
        <Modal open={open} onClose={handleClose}>
          <Box marginTop="15vh" mx="15vw" className={classes.modalbox}>
            <UnicornPopUp
              unicornName={unicorn.name}
              unicornColour={unicorn.colour}
              unicornAge={unicorn.age}
              id={unicorn._id}
              onDialogClose={handleClose}
            />
          </Box>
        </Modal>
      </Grid>
    </>
  );
};
