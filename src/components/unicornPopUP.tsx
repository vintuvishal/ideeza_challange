import {
    Box,
    Button,
    Paper,
    Typography,
    IconButton,
    TextField,
  } from "@material-ui/core";
  import React from "react";
  import CloseIcon from "@material-ui/icons/Close";
  import { useForm, Controller } from "react-hook-form";
  import { useDispatch } from "react-redux";
  import { createUnicorn, updateUnicorn } from "../redux/actions/data";
  
  export const UnicornPopUp = (props: any) => {
    const { unicornName, unicornColour, unicornAge, id, onDialogClose } = props;
    const { handleSubmit, control } = useForm();
    const dispatch = useDispatch();
  
    const handleCreateUnicorn = (data: any) => {
      // handleCreateTodo({name, picture}, setError, onDialogClose)
      dispatch(createUnicorn(data));
      onDialogClose();
    };
  
    const handleEditUnicorn = (data: any) => {
      // handleEditTodo({name, picture}, todoId, onDialogClose, index)
      console.log();
      dispatch(updateUnicorn(data, id));
      onDialogClose();
      // onDialogClose()
    };
  
    return (
      <Paper elevation={3}>
        <Box display="flex" flexDirection="row-reverse" px={4} pt={4}>
          <IconButton aria-label="delete" onClick={onDialogClose}>
            <CloseIcon color="inherit" />
          </IconButton>
        </Box>
        <Box
          display="flex"
          flexDirection="column"
          alignItems="center"
          justifyContent="center"
          pb={4}
        >
          <Typography variant="h4">
            {unicornName ? "Edit Unicorn" : "Create new Unicorn"}
          </Typography>
          <form
            onSubmit={handleSubmit(
              unicornName ? handleEditUnicorn : handleCreateUnicorn
            )}
          >
            <Box mt={3}>
              <Controller
                name="name"
                control={control}
                defaultValue={unicornName ? unicornName : ""}
                render={({
                  field: { onChange, value },
                  fieldState: { error },
                }) => (
                  <TextField
                    label="Unicorn Name*"
                    variant="filled"
                    value={value}
                    onChange={onChange}
                  />
                )}
                rules={{ required: "Unicorn name required" }}
              />
            </Box>
            <Box mt={3}>
              <Controller
                name="age"
                control={control}
                defaultValue={unicornAge ? unicornAge : ""}
                render={({
                  field: { onChange, value },
                  fieldState: { error },
                }) => (
                  <TextField
                    label="Unicorn Age*"
                    variant="filled"
                    value={value}
                    onChange={onChange}
                  />
                )}
                rules={{ required: "Unicorn Age requried" }}
              />
            </Box>
            <Box mt={3}>
              <Controller
                name="colour"
                control={control}
                defaultValue={unicornColour ? unicornColour : ""}
                render={({
                  field: { onChange, value },
                  fieldState: { error },
                }) => (
                  <TextField
                    label="Unicorn Colour*"
                    variant="filled"
                    value={value}
                    onChange={onChange}
                  />
                )}
                rules={{ required: "Unicorn colour required" }}
              />
            </Box>
            <Box mt={3}>
              <Button type="submit" variant="contained" color="primary">
                {unicornName ? "Edit Unicorn" : "Create Unicorn"}
              </Button>
            </Box>
          </form>
        </Box>
      </Paper>
    );
  };
  