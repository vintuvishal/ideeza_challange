import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { getData } from "../redux/actions/data";
import {
  Box,
  Typography,
  Modal,
  Grid,
  makeStyles,
  Button,
} from "@material-ui/core";
import { RootState } from "../redux/reducers/index";
import { UnicornItem } from "./unicornItem";
import { UnicornPopUp } from "./unicornPopUP";

const useStyles = makeStyles(({ palette }) => ({
  modalbox: {
    outline: "none",
  },
  fieldBox: {
    width: "170px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

function App() {
  const dispatch = useDispatch();
  const unicorns = useSelector((state: RootState) => state.data.data);
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  React.useEffect(() => {
    dispatch(getData());
  }, [dispatch]);

  return (
    <>
      <Typography variant="h4" align="center">
        CRUD Unicorn App
      </Typography>
      <Box display="flex" flexDirection="row-reverse" px={2}>
        <Button variant="contained" color="primary" onClick={handleOpen}>
          Create Unicorn
        </Button>
      </Box>
      <Grid container direction="row" alignItems="center" justify="center">
        <Box className={classes.fieldBox}>
          <Typography>Name</Typography>
        </Box>
        <Box className={classes.fieldBox}>
          <Typography>Age</Typography>
        </Box>
        <Box className={classes.fieldBox}>
          <Typography>Colour</Typography>
        </Box>
        <Box className={classes.fieldBox}>
          <Typography>Edit/Delete</Typography>
        </Box>
      </Grid>
      {unicorns.length > 0 &&
        unicorns.map((unicorn: any, index: number) => {
          return <UnicornItem unicorn={unicorn} index={index} />;
        })}
      <Modal open={open} onClose={handleClose}>
        <Box marginTop="15vh" mx="15vw" className={classes.modalbox}>
          <UnicornPopUp onDialogClose={handleClose} />
        </Box>
      </Modal>
    </>
  );
}

export default App;
