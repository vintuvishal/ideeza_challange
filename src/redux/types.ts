export const GET_DATA = 'GET_DATA';
export const GET_DATA_REQUESTED = 'GET_DATA_REQUESTED';
export const GET_DATA_SUCCESS = 'GET_DATA_SUCCESS';
export const GET_DATA_FAILED = 'GET_DATA_FAILED';
export const CREATE_UNICORN_REQUESTED = 'CREATE_UNICORN_REQUESTED';
export const UPDATE_UNICORN_REQUESTED = 'UPDATE_UNICORN_REQUESTED';
export const DELETE_UNICORN_REQUESTED = 'DELETE_UNICORN_REQUESTED';
