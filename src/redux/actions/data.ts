import * as type from '../types';

export function getData() {
    return {
      type: type.GET_DATA_REQUESTED,
    }
  }

export function createUnicorn(data: any) {
    return {
      type: type.CREATE_UNICORN_REQUESTED,
      data
    }
}

export function updateUnicorn(data: any, id: any) {
    return {
      type: type.UPDATE_UNICORN_REQUESTED,
      data,
      id
    }
}

export function deleteUnicorn(id: any) {
    return {
      type: type.DELETE_UNICORN_REQUESTED,
      id
    }
}