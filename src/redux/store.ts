import { createStore, applyMiddleware, compose } from 'redux';

import createSagaMiddleware from 'redux-saga';
import rootReducer from './reducers/index';
import rootSaga from './sagas/index'

const sagaMiddleware = createSagaMiddleware();
const store = compose<any>(
    applyMiddleware(sagaMiddleware),
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__() 
  )(createStore)(rootReducer);

  sagaMiddleware.run(rootSaga);

export default store;





