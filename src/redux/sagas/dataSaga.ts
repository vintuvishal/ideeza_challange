import { call, put, takeEvery, all } from "redux-saga/effects";

const apiUrl =
  "https://crudcrud.com/api/b46af7bbb27c449b9741334f4d3041c0/unicorns";
const getData = () => {
  return fetch(apiUrl, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .catch((error) => {
      throw error;
    });
};

const createUnicorn = (data: any) => {
  return fetch(apiUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then(getData)
    .catch((error) => {
      throw error;
    });
};

const updateUnicorn = (data: any, id: number) => {
  return fetch(`${apiUrl}/${id}`, {
      method: 'PUT',
      headers: {
          'Content-Type': 'application/json',
    
      },
      body: JSON.stringify(data)
  }).then(getData)
    .catch((error) => {throw error})
  
};

const deleteUnicorn = (id: number) => {
  return fetch(`${apiUrl}/${id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then(getData)
    .catch((error) => {
      throw error;
    });
};

interface ResponseGenerator {
  config?: any;
  data?: any;
  headers?: any;
  request?: any;
  status?: number;
  statusText?: string;
}

function* readData(action: any) {
  try {
    const data: ResponseGenerator = yield call(getData);
    yield put({ type: "GET_DATA_SUCCESS", data: data });
  } catch (e) {
    yield put({ type: "GET_DATA_FAILED", message: e.message });
  }
}

function* createData(action: any) {
  try {
    const data: ResponseGenerator = yield call(createUnicorn, action.data);
    yield put({ type: "GET_DATA_SUCCESS", data: data });
  } catch (e) {
    yield put({ type: "GET_DATA_FAILED", message: e.message });
  }
}
function* updateData(action: any) {
  try {
    const data: ResponseGenerator = yield call(
      updateUnicorn,
      action.data,
      action.id
    );
    yield put({ type: "GET_DATA_SUCCESS", data: data });
  } catch (e) {
    yield put({ type: "GET_DATA_FAILED", message: e.message });
  }
}
function* deleteData(action: any) {
  try {
    const data: ResponseGenerator = yield call(deleteUnicorn, action.id);
    yield put({ type: "GET_DATA_SUCCESS", data: data });
  } catch (e) {
    yield put({ type: "GET_DATA_FAILED", message: e.message });
  }
}

function* getdataSaga() {
  yield takeEvery("GET_DATA_REQUESTED", readData);
}

function* createDataSaga() {
  yield takeEvery("CREATE_UNICORN_REQUESTED", createData);
}

function* updateDataSaga() {
  yield takeEvery("UPDATE_UNICORN_REQUESTED", updateData);
}

function* deleteDataSaga() {
  yield takeEvery("DELETE_UNICORN_REQUESTED", deleteData);
}

export default function* rootSaga() {
  yield all([
    getdataSaga(),
    createDataSaga(),
    updateDataSaga(),
    deleteDataSaga(),
  ]);
}
