import { combineReducers } from 'redux';
import data from './data';

const rootReducer = combineReducers({
  data: data,
});

export type RootState = ReturnType<typeof rootReducer>
export default rootReducer;